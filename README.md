# GitBook LibreJS Theme

This is the default theme for GitBook since version `3.0.0`. It can be used as a template for theming books or can be extended.

This is a fork of the [default theme](https://github.com/GitbookIO/theme-default) with LibreJS support added. The following files were newly added:

* `src/js/librejs/core.js`
* `src/js/librejs/theme.js`

and the following files were altered:

* `src/build.sh`

![Image](https://raw.github.com/GitbookIO/theme-default/master/preview.png)
